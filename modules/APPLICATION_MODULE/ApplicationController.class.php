<?php

/**
 * 
 * Authors:
 *  - Vladimirovna
 * 
 * @Instance
 * 
 * Commands this controller contains:
 *	@DefineCommand(
 *		command     = "apply", 
 *		accessLevel = "all", 
 *		description = "Create a new application if one does not already exist", 
 *		help        = "application_apply.txt"
 *	)
 *	@DefineCommand(
 *		command     = "applist", 
 *		accessLevel = "mod", 
 *		description = "List of applicants", 
 *		help        = "application_list.txt"
 *	)
 *	@DefineCommand(
 *		command     = "appopen", 
 *		accessLevel = "mod", 
 *		description = "Open specific application", 
 *		help        = "application_open.txt"
 *	)
 *	@DefineCommand(
 *		command     = "appcomment", 
 *		accessLevel = "mod", 
 *		description = "Add a comment to an application", 
 *		help        = "application_comment.txt"
 *	)
 *	@DefineCommand(
 *		command     = "appremove", 
 *		accessLevel = "all", 
 *		description = "Remove personal application if it's in applied or processing state", 
 *		help        = "application_remove.txt"
 *	)
 *	@DefineCommand(
 *		command     = "appadminremove", 
 *		accessLevel = "mod", 
 *		description = "Remove specific application", 
 *		help        = "application_adminremove.txt"
 *	)
 *	@DefineCommand(
 *		command     = "appupdate", 
 *		accessLevel = "all", 
 *		description = "Updates your application", 
 *		help        = "application_update.txt"
 *	)
 *	@DefineCommand(
 *		command     = "appsetstatus", 
 *		accessLevel = "mod", 
 *		description = "Updates an existing application's status", 
 *		help        = "application_setstatus.txt"
 *	)
 *	@DefineCommand(
 *		command     = "appstatus", 
 *		accessLevel = "all", 
 *		description = "See current status of application", 
 *		help        = "application_status.txt"
 *	)
 *	@DefineCommand(
 *		command     = "recruiters", 
 *		accessLevel = "all", 
 *		description = "See list of recruiters", 
 *		help        = "application_recruiters.txt"
 *	)
 *	@DefineCommand(
 *		command     = "addrecruiter", 
 *		accessLevel = "mod", 
 *		description = "Add recruiter", 
 *		help        = "application_addrecruiter.txt"
 *	)
 *	@DefineCommand(
 *		command     = "remrecruiter", 
 *		accessLevel = "mod", 
 *		description = "Remove a recruiter", 
 *		help        = "application_remrecruiter.txt"
 *	)
 *	@DefineCommand(
 *		command     = "addrecruiteralts", 
 *		accessLevel = "mod", 
 *		description = "Add recruiter's alts", 
 *		help        = "application_addrecruiteralts.txt"
 *	)
 *	@DefineCommand(
 *		command     = "remrecruiteralts", 
 *		accessLevel = "mod", 
 *		description = "Removes recruiter alt", 
 *		help        = "application_remrecruiteralt.txt"
 *	)
 *	@DefineCommand(
 *		command		= "appspam",
 *		accessLevel = "mod",
 *		description = "Spams recruiter poster manually",
 *		help		= "application_spam.txt"
 *	)
 *	@DefineCommand(
 *		command		= "ra",
 *		accessLevel = "mod",
 *		description = "Sends a message to all recruiters",
 *		help		= "application_alert.txt"
 *	)
 *
 */

class ApplicationController {
	
	public $moduleName;
	
	/** @Inject */
	public $db;
	
	/** @Inject */
	public $chatBot;
	
	/** @Inject */
	public $text;
	
	/** @Inject */
	public $buddylistManager;
	
	/** @Inject */
	public $playerManager;
	
	/** @Inject */
	public $settingManager;
	
	/**
	 * @Setup
	 */
	public function setup() {
		$this->db->loadSQLFile($this->moduleName, "applicants");
		
		$this->settingManager->add($this->moduleName, "new_application_notify", "When enabled, all recruiters will be notified of new applications", "edit", "options", "1", "on;off", "1;0");
	}
	
	/**
	 * Creates a new application if one does not exist already.
	 * 
	 * @param string $name - name of applicant
	 * @return bool - true if application was added
	 */
	public function apply($name, $alts) {
		// Count if applicant name exists
		$sql = "SELECT COUNT(*) as tot FROM (SELECT a.name, b.alt_name FROM applicants a, applicants_alts b WHERE a.name = ? OR b.alt_name = ?)";
		$count = $this->db->query($sql, $name, $name);
		
		// If applicant does not exist, create new application
		if($count[0]->tot == 0) {
			// Insert main into applicants table
			$now = time();
			
			$sql = "INSERT INTO applicants (name, status, comment, applied, recruited) VALUES (?,?,?,?,?)";
			
			// We add empty string to "comments" field
			$this->db->exec($sql, $name, 0, "", $now, $now);
			
			// Attempt to add alts
			$sql = "INSERT INTO applicants_alts (main_name, alt_name) VALUES (?, ?)";
			
			forEach($alts as $val) {
				$this->db->exec($sql, $name, ucfirst(strtolower($val)));
			}
			
			// Return true to indicate that main/alts were added successfully
			return true;
		}
		
		// If count is more than 1, then applicant already has an application in place - return false
		return false;
	}
	
	/**
	 * Adds a comment to an application
	 * 
	 * @param string $advisor - who made the comment
	 * @param string $comment - the comment
	 * @param int $id - id of application
	 */
	public function appcomment($advisor, $comment, $id) {
		// Create comment
		$msg = "{$advisor}: <highlight>{$comment}<end>\n";
		
		// Concatonate already existing comments with new
		$sql = "UPDATE applicants SET comment = comment || ? WHERE id = ?";
		
		// Add/execute to db
		$this->db->exec($sql, $msg, $id);
	}
	
	/**
	 * Removes an application. Can only be done if it is in the state of being processed or has just been submitted (status = applied).
	 * 
	 * @param int $id - application id
	 */
	public function appremove($id, $isAdmin) {
		$sql = "SELECT a.name FROM applicants a WHERE id = ?";
		$result = $this->db->query($sql, $id);
		
		$name = $result[0]->name;
		
		if($isAdmin) {
			// Removes application as if admin was removing (no restrictions)
			$sql = "DELETE FROM applicants WHERE id = ?";
		} else {
			// Removes application if status IS NOT "recruit" (2) or "denied" (3)
			$sql = "DELETE FROM applicants WHERE id = ? AND (status != 2 OR status != 3)";
		}
		
		// Add/execute to db
		$this->db->exec($sql, $id);
		
		$sql = "DELETE FROM applicants_alts WHERE main_name = ?";
		
		$this->db->exec($sql, $name);
	}
	
	/**
	 * Lists the current applicants.
	 * 
	 * @param string $status - what state the applications must be in 
	 * @return string - containing the list of applications
	 */
	public function applist($status) {
		// Get list of applicants
		$sql = "SELECT a.id, a.name, a.status FROM applicants a";
		
		$blob = "";
		
		// Indicate what state the applications must be in
		if($status == 4) {
			$sql .= " WHERE a.status = ? OR a.status = ? ORDER BY applied DESC";
			$result = $this->db->query($sql, 0, 1);
		} else {
			$sql .= " WHERE a.status = ? ORDER BY applied DESC";
			$result = $this->db->query($sql, $status);
		}
		
		// Iterate through result set from DB
		forEach($result as $row) {
			// Character information
			$id = $row->id;
			$name = $row->name;
			$ustatus = $row->status;
			
			// Character lookup (People of RK)
			$whois = $this->playerManager->get_by_name($name);
			
			// Application link
			$appLink = $this->text->makeChatcmd("Open application", "/tell <myname> appopen {$id}");
			
			// Create content
			$blob .= $this->playerManager->get_info($whois);
			$blob .= " :: " . $this->formatStatus($ustatus);
			$blob .= " :: " . $appLink . "\n\n";
		}
			
		return $blob;
	}
	
	/**
	 * Updates an applicant's list of alts
	 * 
	 * @param string[] $alts - array of alts
	 * @param string $main - name of main character
	 */
	public function appupdate($alts, $main) {
		$sql = "SELECT b.alt_name as name FROM applicants_alts b WHERE b.main_name = ?";
		$sqlCount = "SELECT COUNT(*) as tot FROM (SELECT b.alt_name FROM applicants_alts b WHERE b.alt_name = ?)";
		
		$result = $this->db->query($sql, ucfirst(strtolower($main)));
		
		// If no alts were added initially, remove the "NONE"-entry
		forEach($result as $row) {
			if($row->name == "NONE") {
				$sql = "DELETE FROM applicants_alts b WHERE b.main_name = ?";
				$this->db->exec($sql, $main);
			}
		}
		
		// Insert each new alt added to table
		$sql = "INSERT INTO applicants_alts (main_name, alt_name) VALUES (?,?)";
		forEach($alts as $val) {
			$count = $this->db->query($sqlCount, $val);
			
			if($count[0]->tot == 0) {
				$this->db->exec($sql, $main, ucfirst(strtolower($val)));
			}
		}
	}
	
	/**
	 * Creates an info window containing a spcific application given with param aid
	 * 
	 * @param int $aid - id of the application
	 * @return string[] - main name and AOML of application
	 */
	public function appopen($aid) {
		$sql = "SELECT a.id, a.name, a.comment, a.status, b.alt_name, 
					   datetime((SELECT a.applied FROM applicants a WHERE a.id = ?),'unixepoch')
					   	as adate, 
					   datetime((SELECT a.recruited FROM applicants a WHERE a.id = ?),'unixepoch')
						as rdate 
				FROM applicants a 
				JOIN applicants_alts b ON a.name = b.main_name 
				WHERE a.id = ? 
				ORDER BY adate DESC";
		$result = $this->db->query($sql, $aid, $aid, $aid);
		
		$uname = $result[0]->name;
		$comment = "";
		
		$blob = "";
		
		$main = true;
		
		forEach($result as $row) {
			if($main) {
				$main = false;
				
				// User information
				$id = $row->id;
				$name = $row->name;
				$status = $row->status;
				$comment = $row->comment;
				$adate = $row->adate;
				$rdate = $row->rdate;
				
				// Online and lookup
				$online = $this->getOnline($name);
				$whois = $this->playerManager->get_by_name($name);
				
				// Quick links
				$setApplied = $this->text->makeChatcmd("applied", "/tell <myname> appsetstatus $id applied");
				$setProc = $this->text->makeChatcmd("processing", "/tell <myname> appsetstatus $id proc");
				$setRecruit = $this->text->makeChatcmd("recruit", "/tell <myname> appsetstatus $id recruit");
				$setDenied = $this->text->makeChatcmd("deny", "/tell <myname> appsetstatus $id denied");
				
				// Create a tell-link if user is online
				$nameLink = $this->getNameLink($name, $online);
				
				// Set recruit date
				if($adate == $rdate) {
					$rdate = "Not recruited/denied";
				}
				
				// Main character information
				$blob = "Current status: " . $this->formatStatus($status) . "\n";
				$blob .= "Applied: <highlight>$adate<end>\n";
				$blob .= "Recruited/denied: <highlight>$rdate<end>\n";
				$blob .= "Set status: [$setApplied] [$setProc] [$setRecruit] [$setDenied]\n\n";
				$blob .= "Name: <highlight>{$whois->firstname} \"{$nameLink}\" {$whois->lastname}<end> {$lookupNameLink}\n";
				$blob .= "Breed: <highlight>{$whois->breed}<end>\n";
				$blob .= "Gender: <highlight>{$whois->gender}<end>\n";
				$blob .= "Profession: <highlight>{$whois->profession} (" . trim($whois->prof_title) . ")<end>\n";
				$blob .= "Level: <highlight>{$whois->level}<end>\n";
				$blob .= "AI Level: <highlight>{$whois->ai_level} ({$whois->ai_rank})<end>\n";
				$blob .= "Faction: <highlight>{$whois->faction}<end>\n";
				$blob .= "Online status: " . $this->formatOnline($online) . "\n";
				$blob .= "Lookup history: " . $this->text->makeChatcmd("History", "/tell <myname> history {$name}") . "\n";
				$blob .= "Character ID: <highlight>{$whois->charid}<end> {$lookupCharIdLink}\n\n";
				
				// Add alts (if any)
				$blob .= "Registered alts:\n";
				if($row->alt_name != null && $row->alt_name != "NONE") {
					$name = $row->alt_name;
					$online = $this->getOnline($name);
					
					$blob .= $this->formatAlt($name, $online);
				} else {
					$blob .= "<highlight>None<end>\n";
				}
			} else {
				$name = $row->alt_name;
				$online = $this->getOnline($name);
					
				$blob .= $this->formatAlt($name, $online);
			}
		}
		
		$blob .= "\nComments (Advisors):\n";
		if($comment == "" || $comment == null) {
			$blob .= "<highlight>None<end>\n";
		} else {
			$blob .= $comment;
		}
		
		$blob .= "\nNotes\n";
		$blob .= "<highlight>Use <symbol>appcomment $aid &lt;comment&gt;<end> to add new comments.\n";
		$blob .= "<highlight>The applicant can read these comments.";
		
		return array($uname, $blob);
	}
	
	/**
	 * Updates the status of an application.
	 * 
	 * @param int $id - application id 
	 * @param string $status - new state of application
	 */
	public function appsetstatus($id, $status) {
		// Format status
		switch($status) {
			case "applied":
				$sql = "UPDATE applicants SET status = ?, recruited = (SELECT applied FROM applicants WHERE id = ?) WHERE id = ?";
				$this->db->exec($sql, 0, $id, $id);
				break;
			case "proc":
				$sql = "UPDATE applicants SET status = ?, recruited = (SELECT applied FROM applicants WHERE id = ?) WHERE id = ?";
				$this->db->exec($sql, 1, $id, $id);
				break;
			case "recruit":
				$now = time();
				$sql = "UPDATE applicants SET status = ?, recruited = ? WHERE id = ?";
				$this->db->exec($sql, 2, $now, $id);
				break;
			case "denied":
				$now = time();
				$sql = "UPDATE applicants SET status = ?, recruited = ? WHERE id = ?";
				$this->db->exec($sql, 3, $now, $id);
				break;
		}
		
		// Notify applicant
		$this->notifyapplicant($id);
	}
	
	/**
	 * Create an AOML info window that includes the current status of application and any comments from advisor(s)
	 *  
	 * @param string $main - name of the applicant
	 * @return string - AOML info window with current status
	 */
	public function appstatus($main) {
		// Get application information
		$sql = "SELECT a.status, a.comment FROM applicants a WHERE a.name = ?";
		$result = $this->db->query($sql, $main);
		
		// Create application status content
		$blob = "Current status of your application: ";
		$blob .= $this->formatStatus($result[0]->status) . "\n\n";
		$blob .= "Comments from Advisor(s):\n";
		$blob .= $result[0]->comment;
		
		return $blob;
	}
	
	/**
	 * Creates a list of recruiters and their alts. Uses AOML and will show whether a recruiter is online or not.
	 * 
	 * @return string - AOML info window with recruiters
	 */
	public function apprecruiters() {
		// Select all recruiters and their alts
		$sql = "SELECT a.id, a.name, b.alt_name FROM recruiters a JOIN recruiters_alts b ON a.name = b.main_name";
		$result = $this->db->query($sql);
		
		$blob = "";
		
		$currentId = 0;
		
		// Create info window with each recruiter an their alts
		forEach($result as $row) {
			if($currentId != $row->id) {
				$currentId = $row->id;
		
				$name = $row->name;
				$online = $this->getOnline($name);
				$whois = $this->playerManager->get_by_name($name);
		
				$blob .= $this->getNameLink($name, $online);
				$blob .= " :: " . $this->formatOnline($online) . "\n";
					
				if($row->alt_name != null || $row->alt_name != "NONE") {
					$name = $row->alt_name;
					$online = $this->getOnline($name);
						
					$blob .= "<tab>" . $this->getNameLink($name, $online);
					$blob .= " :: " . $this->formatOnline($online) . "\n";
				}
			} else {
				$name = $row->alt_name;
				$online = $this->getOnline($name);
					
				$blob .= "<tab>" . $this->getNameLink($name, $online);
				$blob .= " :: " . $this->formatOnline($online) . "\n";
			}
		}
		
		return $blob;
	}
	
	/**
	 * Adds one or more new recruiters.
	 * 
	 * @param string[] $recruiters - list of recruiters
	 */
	public function addrecruiter($recruiters) {
		$sql = "INSERT INTO recruiters (name) VALUES(?)";
		$sqlAlts = "INSERT INTO recruiters_alts (main_name, alt_name) VALUES(?,?)";
		$sqlCheck = "SELECT COUNT(*) as tot FROM (SELECT a.name FROM recruiters a WHERE a.name = ?)";
		
		$blob = "";
		
		forEach($recruiters as $val) {
			$name = ucfirst(strtolower($val));
			
			$result = $this->db->query($sqlCheck, $name);
			
			if($result[0]->tot == 0) {
				$this->db->exec($sql, $name);
				$this->db->exec($sqlAlts, $name, 'NONE');
				$blob .= "$name<tab><tab>added successfully\n";
			} else {
				$blob .= "$name<tab><tab>already exists in the DB\n";
			}
		}
		
		return $blob;
	}
	
	/**
	 * Removes one or more existing recruiters.
	 * 
	 * @param string[] $recruiters - list of recruiters
	 */
	public function remrecruiter($recruiters) {
		$sql = "DELETE FROM recruiters WHERE name = ?";
		$sqlAlts = "DELETE FROM recruiters_alts WHERE main_name = ?";
		$sqlCheck = "SELECT COUNT(*) as tot FROM (SELECT a.name FROM recruiters a WHERE a.name = ?)";
		
		$blob = "";
		
		forEach($recruiters as $val) {
			$name = ucfirst(strtolower($val));
				
			$result = $this->db->query($sqlCheck, $name);
				
			if($result[0]->tot == 1) {
				$this->db->exec($sql, $name);
				$this->db->exec($sqlAlts, $name);
				
				$blob .= "$name<tab><tab>removed successfully (including alts if any)\n";
			} else {
				$blob .= "$name<tab><tab>does not exist in the DB\n";
			}
		}
		
		return $blob;
	}
	
	/**
	 * Adds new recruiter alts
	 * 
	 * @param string $name - main name
	 * @param string $alts - list of alts
	 * @return string - AOML window of success
	 */
	public function addrecruiteralts($name, $alts) {
		$char = ucfirst(strtolower($name));
		
		$sqlAddAlt = "INSERT INTO recruiters_alts (main_name, alt_name) VALUES(?,?)";
		$sqlCheck = "SELECT COUNT(*) as tot, b.main_name as name FROM (SELECT a.name, b.alt_name FROM recruiters a, recruiters_alts b WHERE a.name = ? OR b.alt_name = ?), recruiters_alts b WHERE b.main_name = ? OR b.alt_name = ?";
		
		$blob = "";
		
		$result = $this->db->query($sqlCheck, $char, $char, $char, $char);
		
		if($result[0]->tot > 0) {
			$main = $result[0]->name;
			
			// Get list of alts
			$sqlAlts = "SELECT b.alt_name as name FROM recruiters_alts b WHERE b.main_name = ?";
			$altList = $this->db->query($sqlAlts, $main);
			
			forEach($alts as $val) {
				$alt = ucfirst(strtolower($val));
				$notPresent = true;
				
				$len = count($altList);
				
				for($i = 0; $i < $len && $notPresent; $i++) {
					if($altList[$i]->name == $alt) {
						$notPresent = false;
					}
				}
				
				if($notPresent) {
					$this->db->exec($sqlAddAlt, $main, $alt);
						
					$blob .= "$alt<tab><tab>added successfully\n";
				} else {
					$blob .= "$alt<tab><tab>already added as alt\n";
				}
			}
		} else {
			$blob .= "$char<tab><tab>does not exist in the DB\n";
		}
		
		return $blob;
	}
	
	/**
	 * Remove alts of recruiter
	 * 
	 * @param string $name - recruiter
	 * @param string $alts - list of alts
	 * @return string - AOML window of success
	 */
	public function remrecruiteralts($name, $alts) {
		$main = ucfirst(strtolower($name));
	
		$sql = "DELETE FROM recruiters_alts WHERE main_name = ? AND alt_name = ?";
		$sqlCheck = "SELECT COUNT(*) as tot FROM (SELECT a.name FROM recruiters a WHERE a.name = ?)";
	
		$blob = "";
	
		$result = $this->db->query($sqlCheck, $main);
	
		if($result[0]->tot == 1) {
			forEach($alts as $val) {
				$alt = ucfirst(strtolower($val));
	
				$this->db->exec($sql, $main, $alt);
	
				$blob .= "$alt<tab><tab>removed successfully (if present)\n";
			}
		} else {
			$blob .= "$main<tab><tab>does not exist in the DB\n";
		}
	
		return $blob;
	}
	
	/**
	 * Notifies all recruiters of new application
	 * 
	 * @param string $name - name of applicant
	 */
	public function notifyrecruiters($name, $id) {
		$sql = "SELECT a.id, a.name, b.alt_name FROM recruiters a, recruiters_alts b WHERE a.name = b.main_name";
		$result = $this->db->query($sql);
		
		$currentId = 0;
		$len = count($result);
		$counter = 0;
		
		$application = $this->text->makeBlob($name, $this->text->makeChatcmd("Open application", "/tell <myname> appopen $id"));
		$msg = "<font color='#FF0000'>[RoD alert]</font> New application by <font color='#808080'>[</font>$application<font color='#808080'>]</font>";
		
		for($i = 0; $i < $len; $i++) {
			if($currentId == $result[$i]->id) {
				if($this->getOnline($result[$i]->alt_name) === 1) {
					$recruiters[$counter] = $result[$i]->alt_name;
					$msgs[$counter] = $msg;
					$counter++;
				}
			} else {
				$currentId = $result[$i]->id;
		
				if($this->getOnline($result[$i]->name) === 1) {
					$recruiters[$counter] = $result[$i]->name;
					$msgs[$counter] = $msg;
					$counter++;
				}
		
				if($result[$i]->alt_name != "" || $result[$i]->alt_name != null || $result[$i]->alt_name != "NONE") {
					if($this->getOnline($result[$i]->alt_name) === 1) {
						$recruiters[$counter] = $result[$i]->alt_name;
						$msgs[$counter] = $msg;
						$counter++;
					}
				}
			}
		}
		
		return array($recruiters, $msgs);
	}
	
	/**
	 * Notifies all online recruiters with given message.
	 * 
	 * @param string $name - who made the alert
	 * @param string $msg - the message to destribute
	 */
	public function recruiteralert($name, $msg) {
		$sql = "SELECT a.id, a.name, b.alt_name FROM recruiters a JOIN recruiters_alts b ON a.name = b.main_name";
		$result = $this->db->query($sql);
		
		$recruiters = array();
		$msgs = array();
		$counter = 0;
		
		$currentId = 0;
		$len = count($result);
		
		$alert = "<font color='#FF0000'>[RoD alert]</font> $msg <font color='#808080'>[</font><font color='#C0C0C0'>$name</font><font color='#808080'>]</font>";
		
		for($i = 0; $i < $len; $i++) {
			if($currentId == $result[$i]->id) {
				if($this->getOnline($result[$i]->alt_name) === 1) {
					$recruiters[$counter] = $result[$i]->alt_name;
					$msgs[$counter] = $alert;
					$counter++;
				}
			} else {
				$currentId = $result[$i]->id;
		
				if($this->getOnline($result[$i]->name) === 1) {
					$recruiters[$counter] = $result[$i]->name;
					$msgs[$counter] = $alert;
					$counter++;
				}
		
				if($result[$i]->alt_name != "" || $result[$i]->alt_name != null || $result[$i]->alt_name != "NONE") {
					if($this->getOnline($result[$i]->alt_name) === 1) {
						$recruiters[$counter] = $result[$i]->alt_name;
						$msgs[$counter] = $alert;
						$counter++;
					}
				}
			}
		}
		
		return array($recruiters, $msgs);
	}
	
	/**
	 * @HandlesCommand("apply")
	 * @Matches("/^apply (.+)$/i")
	 * @Matches("/^apply$/i")
	 */
	public function applyCommand($message, $channel, $sender, $sendto, $args) {
		$msg = "Unknown error.";
		
		$alts = explode(" ", $args[1]);
			
		if($this->apply($sender, $alts)) {
			$msg = "Application received. You can check your application status at any time with !appstatus.";
			
			if($this->settingManager->get("new_application_notify")) {
				$sql = "SELECT a.id FROM applicants a WHERE name = ?";
				$result = $this->db->query($sql, $sender);
				
				$id = $result[0]->id;
				
				list($to, $msgs) = $this->notifyrecruiters($sender, $id);
				$this->sendMassTell($to, $msgs);
			}
		} else {
			$msg = "You've already created an application, please use !appstatus to see the status of your application.";
		}
		
		$sendto->reply($msg);
	}
	
	/**
	 * @HandlesCommand("applist")
	 * @Matches("/^applist (applied|proc|recruit|denied)$/i")
	 * @Matches("/^applist$/i")
	 */
	public function appListCommand($message, $channel, $sender, $sendto, $args) {
		$list_type = $args[1];
		
		switch($list_type) {
			case "applied":
				$result = $this->applist(0);
				break;
			case "proc":
				$result = $this->applist(1);
				break;
			case "recruit":
				$result = $this->applist(2);
				break;
			case "denied":
				$result = $this->applist(3);
				break;
			default:
				$result = $this->applist(4);
				break;
		}
		
		$msg = $this->text->makeBlob("Application list", $result);
		
		$sendto->reply($msg);
	}
	
	/**
	 * @HandlesCommand("appopen")
	 * @Matches("/^appopen (\d+)$/i")
	 */
	public function appOpenCommand($message, $channel, $sender, $sendto, $args) {
		$aid = $args[1];
		
		list($name, $blob) = $this->appopen($aid);
		
		$msg = $this->text->makeBlob("$name's Application [{$aid}]", $blob);
		
		$sendto->reply($msg);
	}
	
	/**
	 * @HandlesCommand("appcomment")
	 * @Matches("/^appcomment (\d+) (.+)$/i")
	 */
	public function appCommentCommand($message, $channel, $sender, $sendto, $args) {
		$id = $args[1];
		$comment = $args[2];
		
		$this->appcomment($sender, $comment, $id);
		$this->notifyapplicant($id);
		
		$sendto->reply("Comment added.");
	}
	
	/**
	 * @HandlesCommand("appremove")
	 * @Matches("/^appremove$/i")
	 */
	public function appRemoveCommand($message, $channel, $sender, $sendto, $args) {
		$sql = "SELECT a.name, a.id, a.status FROM applicants a WHERE a.name = ?";
		$result = $this->db->query($sql, $sender);
		
		if($result[0]->status == 0 || $result[0]->status == 1) {
			$this->appremove($result[0]->id, false);
			$sendto->reply("Your application has been removed.");
		} else if($result[0]->status == 2) {
			$sendto->reply("You have already been recruited and can't remove your application, as it is kept for log.");
		} else if($result[0]->status == 3) {
			$sendto->reply("You have already been denied a previous application, and can't remove it - check the reason behind the verdict via !appstatus.");
		}
	}
	
	/**
	 * @HandlesCommand("appadminremove")
	 * @Matches("/^appadminremove (.+)$/i")
	 */
	public function appAdminRemoveCommand($message, $channel, $sender, $sendto, $args) {
		$sql = "SELECT COUNT(*) as tot FROM (SELECT a.id FROM applicants a WHERE a.id = ?)";
		$result = $this->db->query($sql, $args[1]);
		
		if($result[0]->tot > 0) {
			$this->appremove($args[1], true);
			$sendto->reply("Removed application with id = {$args[1]}");
		} else {
			$sendto->reply("No such application with given id ({$args[1]})");
		}
	}
	
	/**
	 * @HandlesCommand("appupdate")
	 * @Matches("/^appupdate (.+)$/i")
	 */
	public function appUpdateCommand($message, $channel, $sender, $sendto, $args) {
		$sql = "SELECT COUNT(*) as tot FROM (SELECT a.id FROM applicants a WHERE a.name = ? AND (status = 0 OR status = 1))";
		$result = $this->db->query($sql, $sender);
		
		$alts = explode(" ", $args[1]);
		
		if($result[0]->tot > 0) {
			$this->appupdate($alts, $sender);
			$sendto->reply("Updated your alt list.");
		} else {
			$sendto->reply("You do not have an active application. Make sure you write the update from the character you originally wrote application from.");
		}
	}
	
	/**
	 * @HandlesCommand("appsetstatus")
	 * @Matches("/^appsetstatus (\d+) (applied|proc|recruit|denied)$/i")
	 */
	public function appSetStatusCommand($message, $channel, $sender, $sendto, $args) {
		$id = $args[1];
		$status = $args[2];
		
		$sql = "SELECT COUNT(*) as tot FROM (SELECT a.id FROM applicants a WHERE id = ?)";
		$result = $this->db->query($sql, $id);
		
		if($result[0]->tot > 0) {
			$this->appsetstatus($id, $status);
			$sendto->reply("Updated status for application [{$id}] to ".strtoupper($status));
		} else {
			$sendto->reply("No such application with given id ({$id})");
		}
	}
	
	/**
	 * @HandlesCommand("appstatus")
	 * @Matches("/^appstatus$/i")
	 */
	public function appStatusCommand($message, $channel, $sender, $sendto, $args) {
		$sql = "SELECT COUNT(*) as tot FROM (SELECT a.id FROM applicants a WHERE a.name = ?)";
		$result = $this->db->query($sql, $sender);
		
		if($result[0]->tot > 0) {
			$msg = $this->appstatus($sender);
			$msg = $this->text->makeBlob("Your application status", $msg);
			
			$sendto->reply($msg);
		} else {
			$sendto->reply("You have no registered application.");
		}
	}
	
	/**
	 * @HandlesCommand("recruiters")
	 * @Matches("/^recruiters$/i")
	 */
	public function appRecruitersCommand($message, $channel, $sender, $sendto, $args) {
		$msg = $this->apprecruiters();
		$msg = $this->text->makeBlob("Recruiters", $msg);
		
		$sendto->reply($msg);
	}
	
	/**
	 * @HandlesCommand("addrecruiter")
	 * @Matches("/^addrecruiter (.+)$/i")
	 */
	public function addRecruiterCommand($message, $channel, $sender, $sendto, $args) {
		$recruiters = explode(" ", $args[1]);
		
		$msg = $this->addrecruiter($recruiters);
		$msg = $this->text->makeBlob("Add recruiter(s) results", $msg);
		
		$sendto->reply($msg);
	}
	
	/**
	 * @HandlesCommand("remrecruiter")
	 * @Matches("/^remrecruiter (.+)$/i")
	 */
	public function remRecruiterCommand($message, $channel, $sender, $sendto, $args) {
		$recruiters = explode(" ", $args[1]);
		
		$msg = $this->remrecruiter($recruiters);
		$msg = $this->text->makeBlob("Remove recruiter(s) results", $msg);
		
		$sendto->reply($msg);
	}
	
	/**
	 * @HandlesCommand("addrecruiteralts")
	 * @Matches("/^addrecruiteralts (.+)$/i")
	 */
	public function addRecruiterAltCommand($message, $channel, $sender, $sendto, $args) {
		$alts = explode(" ", $args[1]);
		
		$msg = $this->addrecruiteralts($sender, $alts);
		$msg = $this->text->makeBlob("Add alt(s) results", $msg);
		
		$sendto->reply($msg);
	}
	
	/**
	 * @HandlesCommand("remrecruiteralts")
	 * @Matches("/^remrecruiteralts (.+)$/i")
	 */
	public function remRecruiterAltCommand($message, $channel, $sender, $sendto, $args) {
		$alts = explode(" ", $args[1]);
		
		$msg = $this->remrecruiteralts($sender, $alts);
		$msg = $this->text->makeBlob("Remove alt(s) results", $msg);
		
		$sendto->reply($msg);
	}
	
	/**
	 * @HandlesCommand("appspam")
	 * @Matches("/^appspam$/i")
	 */
	public function appSpamCommand($message, $channel, $sender, $sendto, $args) {
		$poster = $this->text->makeBlob("Ring of Desctruction is recruiting", "<img src=rdb://45178> <font color=#FF4000>Ring of Destruction</font> <img src=rdb://45178></font><br>Are you looking for an organization with friendly people, a chatty bot and more? this is the place to be!<br><br>What do we offer?<br>- A 24/7 bot.<br>- An orgcity to raid with.<br>- Weekly raid schedules.<br>- Friendly and loyal members.<br>- PvP raids.<br>- Plenty of opportunities to level. <br><br>What do we want in return?<br>- Accepting the rules of the org.<br>- Loyalty to the org and omni side.<br>- You to join the org on all of your characters.<br>- To see you have a good time in the org.<br><br><font color=#F7F8E0>If you wish to apply, click below.<br></font><a href='chatcmd:///tell rodrecruit help apply'>Apply here</a>");
		$pchannel = "OT OOC";
		
		//$this->chatBot->sendPublic($poster, $pchannel);
	}
	
	/**
	 * @Event("45m")
	 * @Description("Post a recruitment poster in OT OOC")
	 */
	public function postRecruitPosterEvent($eventObj) {
		$poster = $this->text->makeBlob("Ring of Desctruction is recruiting", "<img src=rdb://45178> <font color=#FF4000>Ring of Destruction</font> <img src=rdb://45178></font><br>Are you looking for an organization with friendly people, a chatty bot and more? this is the place to be!<br><br>What do we offer?<br>- A 24/7 bot.<br>- An orgcity to raid with.<br>- Weekly raid schedules.<br>- Friendly and loyal members.<br>- PvP raids.<br>- Plenty of opportunities to level. <br><br>What do we want in return?<br>- Accepting the rules of the org.<br>- Loyalty to the org and omni side.<br>- You to join the org on all of your characters.<br>- To see you have a good time in the org.<br><br><font color=#F7F8E0>If you wish to apply, click below.<br></font><a href='chatcmd:///tell rodrecruit help apply'>Apply here</a>");
		$channel = "OT OOC";
		
		//$this->chatBot->sendPublic($poster, $channel);
	}
	
	/**
	 * @HandlesCommand("ra")
	 * @Matches("/^ra (.+)$/i")
	 */
	public function recruiterAlertCommand($message, $channel, $sender, $sendto, $args) {
		list($to, $msg) = $this->recruiteralert($sender, $args[1]);
		
		$this->sendMassTell($to, $msg);
	}
	
	/**
	 * Formats status in colors
	 * 
	 * @param int $status - application status
	 * @return string - AOML formatted status 
	 */
	private function formatStatus($status) {
		switch($status) {
			case 0:
				return "<yellow>applied<end>";
			case 1:
				return "<yellow>processing<end>";
			case 2:
				return "<green>recruited<end>";
			case 3:
				return "<red>denied<end>";
		}
	}
	
	/**
	 * Formats online status
	 * 
	 * @param int $online
	 * @return string - AOML formatted online status
	 */
	private function formatOnline($online) {
		if ($online) {
			return "<green>Online<end>";
		} else {
			return "<red>Offline<end>";
		}
	}
	
	/**
	 * Creates and AOML formatted alt for listing purposes
	 * 
	 * @param string $name - name of alt
	 * @param int $online
	 * @return string - AOML formatted alt info
	 */
	private function formatAlt($name, $online) {
		// Online and lookup for alt
		$whois = $this->playerManager->get_by_name($name);
			
		// Alt history lookup link
		$alt_history = $this->text->makeChatcmd("History", "/tell <myname> history {$name}");
			
		// Alt information
		$blob = "";
		$blob .= $this->playerManager->get_info($whois);
		$blob .= " :: " . $this->formatOnline($online);
		$blob .= " :: " . $alt_history . "\n";
		
		return $blob;
	}
	
	/**
	 * Returns online status.<br>
	 * Null = unknown<br>
	 * 0 = not online<br>
	 * 1 = online  
	 * 
	 * @param string $name - name of character to lookup
	 * @return int - online status
	 */
	private function getOnline($name) {
		$uid = $this->chatBot->get_uid($name);
		
		if ($uid) {
			$online = $this->buddylistManager->isOnline($name);
			if ($online === null) {
				$this->buddylistManager->add($name, 'is_online');
			}
		}
		
		return $online;
	}
	
	/**
	 * Creates a tell-link for given name IF character is online.
	 * 
	 * @param string $name - name of character
	 * @param int $online - online status
	 * @return string - AOML tell-link or just name
	 */
	private function getNameLink($name, $online) {
		if($online) {
			return $this->text->makeChatcmd($name, "/tell {$name}");
		} else {
			return $name;
		}
	}
	
	/**
	 * Sends tells to several receivers.
	 * Both parameters are treated as arrays.
	 * Index 0 in "msg" will be sent to index 0 in "to", and so on.
	 * 
	 * @param string[] $to - list of receivers
	 * @param string[] $msg - list of tell contents for each receiver
	 */
	private function sendMassTell($to, $msg) {
		$len = count($to);
		
		for($i = 0; $i < $len; $i++) {
			$this->chatBot->sendTell($msg[$i], $to[$i]);
		}
	}
	
	/**
	 * Notifies the applicant that there's updates in his application
	 * 
	 * @param int $id - id of application
	 */
	private function notifyapplicant($id) {
		$sql = "SELECT a.id, a.name, b.alt_name FROM applicants a, applicants_alts b WHERE id = ? AND a.name = b.main_name";
		$result = $this->db->query($sql, $id);
		
		$name = $result[0]->name;
		$count = 0;
		
		if($this->getOnline($name)) {
			$to[0] = $name;
			$count++;
		}
		
		$msg = $this->appstatus($name);
		$msg = "Your application has been updated by a recruiter - " . $this->text->makeBlob("Your application status", $msg);
		
		forEach($result as $row) {
			if($this->getOnline($row->alt_name)) {
				$to[$count] = $row->alt_name;
				$count++;
			}
		}
		
		if(count($to) > 0) {
			forEach($to as $val) {
				$this->chatBot->sendTell($msg, $val);
			}
		} else {
			$this->chatBot->sendTell($msg, $name);
		}
	}
}

?>