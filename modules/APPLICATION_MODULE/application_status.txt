<highlight>appstatus<end>
Shows your application's current state.

Example
<highlight><tab><symbol>appstatus<end>

Applied
<highlight>Means that your application has been filed, and a recruiter is yet to look at it.<end>

Proc
<highlight>Means that your application has been seen by a recruiter, and you're in the process of being recruited/checked.<end>

Recruit
<highlight>Means that your application has been accepted, and you're either already invited to org or waiting for a recruiter to invite you.<end>

Denied
<highlight>Means that your application has been denied, and you won't be able to add a new application. A verdict/comment has most likely been given from the recruiter, and if you think that you've been evaluated wrongfully, you can try and contact the recruiter - do not expect this recruiter to change their mind.<end>